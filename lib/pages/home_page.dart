import 'package:flutter/material.dart';
import 'package:todoapp/models/todo_model.dart';
import 'package:todoapp/widgets/todo_item.dart';

class HomePages extends StatefulWidget {
  const HomePages({Key? key}) : super(key: key);

  @override
  _HomePagesState createState() => _HomePagesState();
}

class _HomePagesState extends State<HomePages> {
  List<TodoModel> todos = [];
  TextEditingController todoController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            // NOTE : TITLE

            Text(
              'TODO APP',
              style: TextStyle(
                  color: Colors.white30,
                  fontSize: 20,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 30,
            ),
            // NOTE : TODOLIST
            Expanded(
              child: Column(
                children: todos
                    .map(
                      (item) => TodoItem(
                        title: item.title,
                      ),
                    )
                    .toList(),
              ),
            ),
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(
                bottom: 50,
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              decoration: BoxDecoration(
                color: Colors.white30,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: todoController,
                        // onChanged: (value) {
                        //   print(value);
                        // },
                        decoration:
                            InputDecoration.collapsed(hintText: 'Add Todo...'),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print(todoController.text);
                        setState(() {
                          todos.add(
                            TodoModel(
                              title: todoController.text,
                              isDone: false,
                            ),
                          );

                          todoController.text = '';
                        });
                      },
                      child: Icon(
                        Icons.add,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
